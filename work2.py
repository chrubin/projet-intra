import sys, string
from collections import OrderedDict

def processLine(sin, din):
    """For a given line, append to a dictionary individual words and counts"""
    line = sin.strip()
    words = line.split()
    for word in words:
        
        word = word.translate(string.maketrans("",""), string.punctuation + string.digits)
        word = word.lower()
        
        din[word] = 1 + din.get(word, 0)
    return din


def processFile(fin, processFunc):
    """Return an ordered dictionary of distinct words and occurance counts, along with total word count"""
    d = dict()
    try:
        myFile = open(fin, 'r')
        for line in myFile.readlines():
            processFunc(line, d)
        distinctWords = len(d)
        
        sortedDict = OrderedDict(sorted(d.items(), key=lambda t: t[1]))
        return sortedDict, distinctWords
    except:
        e = sys.exc_info()
        print e
    finally:
        myFile.close()

if __name__ == '__main__':
    bookFile = 'mini.txt'
    infile = 'mino.txt'
    words, distinctWordCount = processFile(bookFile, processLine)
    print words
    print distinctWordCount
#JEAN Cherubin
