# JEAN Cherubin
import string 
 
punctuations = [mark for mark in string.punctuation] 
whitespaces = [char for char in string.whitespace]
 
def words(): 
     data = open('mini.txt', 'r') 
     main = [] 
     for line in data: 
         for item in line.split(): 
             main.append(item) 
     return main 
     data.close()
     
def clean(word): 
     cleansed = '' 
     for char in word: 
         if ((char in punctuations) or (char in whitespaces)): 
             pass 
         else: 
             cleansed += char.lower() 
     return cleansed 
print "The book has %s 'words'" % len([clean(word) for word in words()]) 
