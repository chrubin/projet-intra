import random

def prob(x):
    prob = {}
    for item in x:
        prob[item] = prob.get(item, 0) + 1
    list_ = []
    for key in prob:
        for i in range(0, prob[key]):
            list_.append(key)
    a = 0
    b = 0
    for i in range(0, 10000):
        if random.choice(list_) == 'a':
            a += 1
        else:
            b += 1
    print "a: %.5f" % (a / 10000.0), "b: %.5f" % (b / 10000.0)


if __name__ == "__main__":
    liste1=['a','a','b','b']
    prob=prob(liste1)
    print prob
